Page({
    data: {
        list: [
            {
                id: 'form',
                name: '表单',
                open: false,
                pages: ['button', 'list', 'input', 'slider', 'uploader']
            },
            {
                id: 'widget',
                name: '基础组件',
                open: false,
                pages: ['article', 'badge', 'flex', 'footer', 'gallery', 'grid', 'icons', 'loadmore', 'panel', 'preview', 'progress']
            },
            {
                id: 'feedback',
                name: '操作反馈',
                open: false,
                pages: ['actionsheet', 'dialog', 'msg', 'picker', 'toast']
            },
            {
                id: 'nav',
                name: '导航相关',
                open: false,
                pages: ['navbar', 'tabbar']
            },
            {
                id: 'search',
                name: '搜索相关',
                open: false,
                pages: ['searchbar']
            }
        ],
        clist:[]
    },
    kindToggle: function (e) {
        var id = e.currentTarget.id, list = this.data.list;
        for (var i = 0, len = list.length; i < len; ++i) {
            if (list[i].id == id) {
                list[i].open = !list[i].open
            } else {
                list[i].open = false
            }
        }
        this.setData({
            list: list
        });
    },
    setdata:function(data){
        this.setData({
            clist: data
        });
    },
    onShow:function(e){
        /*
        wx.request({
                url: 'https://www.08nm.com/vip/openapi/1.0/getSpecialty', //仅为示例，并非真实的接口地址
                data: {
                    schoolid: 3 ,
                    sign: 'sdzscqdlokfjsdddff'
                },
                header: {
                    'content-type': 'application/json'
                },
                success: function(res) {
                     page.setdata(res.data.list);
                    //this.data.clist = res.data.list;                    
                    console.log(res.data.list)
                }
        });
        */
    }
});

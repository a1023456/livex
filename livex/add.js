// livex/add.js
Page({
  /**
   * 页面的初始数据
   */
  data: {
    pushurl:'',
    pushkey:'',
    pushlive:'',
    livepushitems:{
        name:'高级选项列表',
        valuelist:{
          mode:{
            cnname:'清晰度',
            name:'mode',
            valuetype:'String',
            valuelist:[
              { name: '实时通话', value: 'RTC', checked: 'true' },
              { name: '标清', value: 'SD', checked:''},
              { name: '高清', value: 'HD', checked: '' },
              { name: '超清', value: 'FHD', checked: '' }
            ]
          },
          autopush: {
            cnname: '自动推流',
            name: 'autopush',
            valuetype: 'Boolean',
            valuelist: [
              { name: '自动推流', value: 'true', checked: 'true' },
              { name: '手动推流', value: 'false', checked: '' }
            ]
          },
          muted: {
            cnname: '声音',
            name: 'muted',
            valuetype: 'Boolean',
            valuelist: [
              { name: '开启声音', value: 'false', checked: 'true' },
              { name: '静音', value: 'true', checked: '' }
            ]
          },
          'enable-camera': {
            cnname: '摄像头',
            name: 'enable-camera',
            valuetype: 'Boolean',
            valuelist: [
              { name: '开启摄像头', value: 'true', checked: 'true' },
              { name: '关闭摄像头', value: 'false', checked: '' }
            ]
          },
          'auto-focus': {
            cnname: '自动聚集',
            name: 'auto-focus',
            valuetype: 'Boolean',
            valuelist: [
              { name: '自动聚焦', value: 'true', checked: 'true' },
              { name: '手动聚焦', value: 'false', checked: '' }
            ]
          },
          orientation: {
            cnname: '方向',
            name: 'orientation',
            valuetype: 'String',
            valuelist: [
              { name: '垂直', value: 'vertical', checked: 'true' },
              { name: '横向', value: 'horizontal', checked: '' }
            ]
          },
          beauty: {
            cnname: '美颜',
            name: 'beauty',
            valuetype: 'Number',
            valuelist: [
              { name: '不开启0', value: '0', checked: 'true' },
              { name: '美颜1', value: '1', checked: '' },
              { name: '美颜5', value: '5', checked: '' },
              { name: '美颜10', value: '10', checked: '' },
              { name: '美颜50', value: '50', checked: '' }
            ]
          },
          whiteness: {
            cnname: '美白',
            name: 'whiteness',
            valuetype: 'Number',
            valuelist: [
              { name: '不开启0', value: '0', checked: 'true' },
              { name: '美白1', value: '1', checked: '' },
              { name: '美白5', value: '5', checked: '' },
              { name: '美白10', value: '10', checked: '' },
              { name: '美白50', value: '50', checked: '' }
            ]
          },
          aspect: {
            cnname: '宽高比',
            name: 'aspect',
            valuetype: 'String',
            valuelist: [
              { name: '9:16', value: '9:16', checked: 'true' },
              { name: '3:4', value: '3:4', checked: '' }
            ]
          }, 
          'background-mute': {
            cnname: '进入后台是否静音',
            name: 'background-mute',
            valuetype: 'Boolean',
            valuelist: [
              { name: '后台静音', value: 'true', checked: 'true' },
              { name: '进入后台不静音', value: 'fasle', checked: '' }
            ]
          }
        }
    }
  },
  radioChange: function (e) {
    console.log(e.detail.value);
    console.log(e.target.id);
    console.log(e);

    var livepushitems = this.data.livepushitems;
    //livepushitems.valuelist[e.target.id].pushvalue = e.detail.value;

    for (var i = 0, len = livepushitems.valuelist[e.target.id].valuelist.length; i < len; ++i) {
      livepushitems.valuelist[e.target.id].valuelist[i].checked = livepushitems.valuelist[e.target.id].valuelist[i].value == e.detail.value;
    }

    this.setData({
      livepushitems: livepushitems
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;

    try {
      wx.getStorage({
        key: 'pushlive',
        success: function (e) {
          console.log('1')
          console.log(e.data)
          that.setData({
            pushurl: e.data.pushurl,
            pushkey: e.data.pushkey,
            pushlive: e.data
          });

        }
      })
    } catch (e) {
    }
/*
    try {
      wx.getStorage({
        key: 'pushurl',
        success: function (e) {
          that.setData({
            pushurl: e.data
          })
        }
      })
    } catch (e) {
    }

    try {
      wx.getStorage({
        key:'pushkey', 
        success:function (e) {
          that.setData({
            pushkey: e.data
          })
        }
      })
    } catch (e) {
    }
*/
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
    console.log('1111'+this.data.livepushitems.valuelist['mode'].pushvalue);

    var pushlive = this.data.pushlive;
    var livepushitems = this.data.livepushitems;
    //livepushitems.valuelist[e.target.id].pushvalue = e.detail.value;
    console.log(pushlive);
    for (var iv in pushlive) {
      console.log(iv);
      if (livepushitems.valuelist[iv] && livepushitems.valuelist[iv].valuelist){
      for (var i = 0, len = livepushitems.valuelist[iv].valuelist.length; i < len; ++i) {
        livepushitems.valuelist[iv].valuelist[i].checked = livepushitems.valuelist[iv].valuelist[i].value == pushlive[iv];
      }
      }
    }

      this.setData({
        livepushitems: livepushitems
      });
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  formSubmit: function (e) {
    //console.log('form发生了submit事件，携带数据为：', e.detail.value);
    wx.setStorage({
      key: "pushlive",
      data: e.detail.value
    });

    wx.getStorage({
      key: 'pushlive',
      success: function (e) {
        console.log(e.data)
      }
    })

    /*
    var pushurl = e.detail.value.pushurl;

    wx.setStorage({
      key: "pushurl",
      data: e.detail.value.pushurl
    });

    wx.setStorage({
      key: "pushlive",
      data: e.detail.value
    });

    wx.getStorage({
      key: 'pushlive',
      success: function (e) {
        console.log(e.data)
      }
    })
    
    if (e.detail.value.pushkey){
      pushurl += ("/" + e.detail.value.pushkey);
      wx.setStorage({
        key: "pushkey",
        data: e.detail.value.pushkey
      })
    }
    pushurl = "push?pushurl=" + encodeURIComponent(pushurl);
    */
    var pushurl = "push";
    //console.log(e.detail.value);
    wx.navigateTo({
     url: pushurl
    })
    
  },
  formReset: function () {
    console.log('form发生了reset事件');
  }
})
// livex/push.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    showcontrols:false,
    controls:{},
    pushurl:"",
    pushlive:{},
    logs:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    
/*
    that.setData(
      {
        pushurl: decodeURIComponent(options.pushurl),
      }
    );
*/
    that.showlog("直播准备中");
    console.log(that.pushlive);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    var that = this;
    //console.log(options.pushurl);
    wx.setKeepScreenOn({
      keepScreenOn: true,
    })
    wx.getStorage({
      key: 'pushlive',
      success: function (e) {
        var pushurl = e.data.pushurl;

        if (e.data.pushkey) {
          pushurl += ("/" + e.data.pushkey);
        }

        that.setData({
          pushlive: e.data,
          pushurl: pushurl
        });

        that.showcontrolbox();
        
        //that.showlog(that.data.autopush);
        that.showlog("直播推流中");
        console.log(e.data);
        //console.log(that.data.pushurl);
      }
    });
    this.pushc = wx.createLivePusherContext();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },
  start(){
    var that = this;
    this.pushc.start({
      success:function(e){
        that.showlog("直播推流中");
      },
      fail: function (e) {
        that.showlog("推流启动 失败");
      }
    });
  },
  stop() {
    var that = this;
    this.pushc.stop({
      success: function (e) {
        that.showlog("停止直播");
      },
      fail: function (e) {
        that.showlog("停止推流 失败");
      }
    });
  },
  pause() {
    var that = this;
    this.pushc.pause({
      success: function (e) {
        that.showlog("直播暂停");
      },
      fail: function (e) {
        that.showlog("直播暂停 失败");
      }
    });
  },
  resume() {
    var that = this;
    this.pushc.resume({
      success: function (e) {
        that.showlog("已恢复直播推流");
      }, 
      fail: function (e) {
        that.showlog("恢复直播推流 失败");
      }
    });
  },
  switchCamera() {
    var that = this;
    that.pushc.switchCamera({
      success: function (e) {
        that.showlog("切换摄像头");
      },
      fail: function (e) {
        that.showlog("切换摄像头 失败");
      }
    });
  },
  contrlbindtap(e){
    var that = this;
    console.log(e);
    console.log(e.target.dataset.id);
    that[e.target.dataset.id]();
    
    /*
    var that = this;
    that.pushc.switchCamera({
      success: function (e) {
        that.showlog("切换摄像头");
      },
      fail: function (e) {
        that.showlog("切换摄像头 失败");
      }
    });
    */
  },
  statechange(e){
    var that = this;
    //console.log(e)
    that.showlog(e.detail.code);
    if (e.detail.code == 1005){
      that.showcontrolbox();
    }
    
  },
  showcontrolbox(vcode){
    var that = this;
    var controls = {
      show: false,
      start: '',
      controllist: {
        start: {
          vid: "start",
          vclass: 'start',
          bindtap: 'start',
          name: '开始',
          show: true,
          code: vcode
        },
        stop: {
          vid: "stop",
          vclass: 'stop',
          bindtap: 'stop',
          name: '停止',
          show: true,
          code: vcode
        }
      }
    };
    //that.showlog('准备更新控制菜单');
    that.setData({
      controls: controls
    });
    //that.showlog('更新控制菜单结束');
    
  },
  showlog(e){
    var that = this;
    //console.log(e);
    var logs = that.data.logs;
    logs.unshift(e);
    logs.splice(8,20);
    that.setData({
      logs: logs
    })
  }
})